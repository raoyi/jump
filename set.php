<?php
/**
 * Created by PhpStorm.
 * 设置要跳转的域名并存入到redis
 * User: huike
 * Date: 17/1/31
 * Time: 上午12:25
 */

$domain = isset($_REQUEST['domain']) ? $_REQUEST['domain'] : '';

if($domain)
{
    $redis = new Redis();
    $redis->connect('127.0.0.1','6379');
    $redis->set('domain',$domain);
    echo 'ok';
}
else
{
    echo 'domain is empty';
}